import * as Types from '../Constants';

const initialState = {
    isTrailerOpen: false,
    urlYoutube: '',
    isBackHomeOpen: false,
};

export const popupReducer = (state = initialState, action) => {
  switch (action.type) {
      case Types.IS_TRAILER_OPEN:
          return {...state, isTrailerOpen: action.payload, urlYoutube: action.url};
      case Types.IS_BACK_HOME_OPEN:
          return {...state, isBackHomeOpen: action.payload};
      default:
          return state;
  }
};
