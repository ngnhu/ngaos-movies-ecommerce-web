import * as Types from '../Constants';

const initialState = {};

export const movieDetailReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.GET_MOVIE_DETAIL:
            return action.payload;
        default:
            return state;
    }
};
