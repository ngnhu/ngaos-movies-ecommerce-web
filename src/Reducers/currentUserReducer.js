import * as Types from '../Constants';

const initialState = [];

export const currentUserReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.SET_CURRENT_USER:
            return action.payload;
        default:
            return state;
    }
};
