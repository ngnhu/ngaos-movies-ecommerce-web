import * as Types from '../Constants';

const initialState = [];

export const selectedSeatReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.SELECTED_SEAT:
            return [...state, action.payload];
        default:
            return state;
    }
};
