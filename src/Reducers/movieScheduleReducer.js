import * as Types from '../Constants';

const initialState = [];

export const movieScheduleReducer = (state = initialState, action) => {
  switch (action.type) {
      case Types.GET_MOVIE_SCHEDULE:
          return action.payload;
      default:
          return state;
  }
};
