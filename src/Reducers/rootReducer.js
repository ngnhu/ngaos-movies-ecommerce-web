import {combineReducers} from "redux";
import {popupReducer} from "./popupReducer";
import {moviesReducer} from "./moviesReducer";
import {movieDetailReducer} from "./movieDetailReducer";
import {movieScheduleReducer} from "./movieScheduleReducer";
import {seatReservedReducer} from "./seatReservedReducer";
import {currentUserReducer} from "./currentUserReducer";
import {selectedSeatReducer} from "./selectedSeatReducer";

const rootReducer = combineReducers ({
    popup: popupReducer,
    movies: moviesReducer,
    movieDetail: movieDetailReducer,
    movieSchedule: movieScheduleReducer,
    seatReserved: seatReservedReducer,
    selectedSeat: selectedSeatReducer,
    currentUser: currentUserReducer
});

export default rootReducer;
