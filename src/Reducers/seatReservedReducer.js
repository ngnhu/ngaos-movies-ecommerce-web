import * as Types from '../Constants';

const initialState = [];

export const seatReservedReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.GET_SEAT_RESERVED:
            return action.payload;
        default:
            return state;
    }
};
