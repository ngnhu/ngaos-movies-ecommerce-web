import * as Types from "../Constants";

const initialState = [];

export const moviesReducer = (state = initialState, action) => {
  switch (action.type) {
      case Types.GET_MOVIES:
          return action.payload;
      default:
          return state;
  }
};
