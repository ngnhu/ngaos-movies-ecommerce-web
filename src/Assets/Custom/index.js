window.addEventListener("load",  () => {
    let menu = document.getElementById('menu');
    let body = document.querySelector('body');
    let menuIcon = document.querySelector('.menu-icon');
    let menuUserIcon = document.querySelector('.menu-user__icon');
    let menuUserAcc = document.querySelector('.menu-user__acc');
    let listBtnMenu = document.querySelectorAll('.btn-hover');
    let btnTop = document.querySelector('.back-to-top');
    let listBtnUserMenu = document.querySelectorAll('.btn-close');
    // btnTop.style.display = 'none';

    if(menuIcon){
        menuIcon.addEventListener('click', () => toggleMenu(body, 'menu-main-active', menuUserAcc));
        menuUserIcon.addEventListener('click', () => toggleUser(body, 'menu-main-active', menuUserAcc));

    }
    menuStickTop(menu, backToTop);
    closeMenu(listBtnMenu, body, 'menu-main-active', menuUserAcc);
    closeUserMenu(listBtnUserMenu, menuUserAcc);
    backToTop(btnTop);

});
const backToTop = (btnTop) => {
    window.addEventListener("scroll", () => {
        const currentScroll = window.scrollY;
        if(currentScroll < 50 || currentScroll === 0){
            btnTop.style.WebkitAnimation = 'fadeOutLeft__menu-user .5s ease';
            btnTop.style.animation = 'fadeOutLeft__menu-user .5s ease';
            setTimeout(function () {
                btnTop.style.display = 'none';
            }, 400)
        }
        else if (currentScroll > 150) {
            btnTop.style.display = 'block';
            btnTop.style.WebkitAnimation = 'fadeInRight__menu-user .5s ease';
            btnTop.style.animation = 'fadeInRight__menu-user .5s ease';
        }
    });
};

const menuStickTop = (menu, backToTop) => {
    const lastScroll = 170;
    window.addEventListener("scroll", () => {
        const currentScroll = window.pageYOffset;
        if (currentScroll === 0) {
            menu.classList.remove('menu__sticky-down');
            return;
        }

        if (currentScroll > lastScroll && !menu.classList.contains('menu__sticky-down')) {
            // down
            menu.classList.remove('menu__sticky-up');
            menu.classList.add('menu__sticky-down');

        } else if (currentScroll < lastScroll && menu.classList.contains('menu__sticky-down')) {
            // up
            menu.classList.remove('menu__sticky-down');
            menu.classList.add('menu__sticky-up');
        }
    });
};

const toggleUser = (body, stringClass, menuUserAcc) => {
    if (body.classList.contains(stringClass)) {
        body.classList.remove(stringClass);
    }
    if (menuUserAcc.style.display === 'block') {
        menuUserAcc.style.WebkitAnimation = 'fadeOutLeft__menu-user .5s ease';
        menuUserAcc.style.animation = 'fadeOutLeft__menu-user .5s ease';
        setTimeout(function () {
            menuUserAcc.style.display = 'none';
        }, 400)
    } else {
        menuUserAcc.style.zIndex = 2;
        menuUserAcc.style.display = 'block';
        menuUserAcc.style.WebkitAnimation = 'fadeInRight__menu-user .5s ease';
        menuUserAcc.style.animation = 'fadeInRight__menu-user .5s ease';
    }
};

const toggleMenu = (body, stringClass, menuUserAcc) => {
    if (body.classList.contains(stringClass)) {
        body.classList.remove(stringClass);
        menuUserAcc.style.display = 'none';
    } else {
        menuUserAcc.style.display = 'none';
        body.classList.add(stringClass);
    }

};

const closeMenu =(listBtnMenu, body, stringClass, menuUserAcc) => {
    for(let btnActive of listBtnMenu){
        btnActive.addEventListener('click', function () {
            if (body.classList.contains(stringClass)) {
                body.classList.remove(stringClass);
            }
            if (menuUserAcc.style.display === 'block') {
                menuUserAcc.style.display = 'none';
            }
        });
    }
};
const closeUserMenu =(listBtnUserMenu, menuUserAcc) => {
    for(let btnClose of listBtnUserMenu){
        btnClose.addEventListener('click', function () {
            if (menuUserAcc.style.display === 'block') {
                menuUserAcc.style.display = 'none';
            }
        });
    }
};
