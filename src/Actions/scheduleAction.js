import api from "../Api";
import * as Types from "../Constants";

export const getMovieSchedule = (idMovie, callback) => {
    return (dispatch) => {
        api
            .get(`showtimes/getShowTimesByMovieId/${idMovie}`)
            .then(res => {
                dispatch({
                    type: Types.GET_MOVIE_SCHEDULE,
                    payload: res.data
                });
                if(callback) callback(res.data)
            })
            .catch(err => console.log(err))
    }
};

