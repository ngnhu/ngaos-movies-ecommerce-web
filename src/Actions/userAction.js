import api from "../Api";
import * as Types from "../Constants";

export const register = (data, callback) => {
    api
        .post('auth/signup', data)
        .then(res => {
            callback();
        })
        .catch(err => console.log(err))
};

export const login = (data, callback) => {
    api
        .post('auth/signin', data)
        .then(res => {
            localStorage.setItem('curnrentUser', JSON.stringify(res.data));
            callback(res.data);
        })
        .catch(err => console.log(err))
};

export const setCurrentUser = (data) => {
    return{
        type: Types.SET_CURRENT_USER,
        payload: data
    }
};
