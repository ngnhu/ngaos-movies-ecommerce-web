import api from "../Api";
import * as Types from "../Constants";

export const getMovies = () => {
  return (dispatch) => {
      api
          .get("movies/getMovie")
          .then(res => {
              dispatch({
                  type: Types.GET_MOVIES,
                  payload: res.data
              })
          })
          .catch(err => console.log(err))
  }
};

export const getMovieDetail = (idMovie, callback) => {
    return (dispatch) => {
        api
            .get(`movies/getMovie/${idMovie}`)
            .then(res => {
                dispatch({
                    type: Types.GET_MOVIE_DETAIL,
                    payload: res.data
                });
                if(callback) callback(res.data)
            })
            .catch(err => console.log(err))
    }
};
