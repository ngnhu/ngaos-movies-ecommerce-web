import api from "../Api";
import * as Types from "../Constants";

export const getSeatReversed = (showTimeId, callback) => {
    return (dispatch) => {
        api
            .get(`booking/getSeatsReservedByShowTimeId/${showTimeId}`)
            .then(res => {
                dispatch({
                    type: Types.GET_SEAT_RESERVED,
                    payload: res.data
                });
                if(callback) callback(res.data)
            })
            .catch(err => console.log(err))
    }
};

export const selectedSeat = (seat) => {
    return {
        type: Types.SELECTED_SEAT,
        payload: seat
    }
};
