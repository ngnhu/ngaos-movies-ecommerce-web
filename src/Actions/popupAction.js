import * as Types from '../Constants';

export const popUpTrailer = (isTrailerOpen, urlYoutube) => {
    return{
        type: Types.IS_TRAILER_OPEN,
        payload: isTrailerOpen,
        url: urlYoutube
    }
};
export const popUpBackHome = (isBackHomeOpen) => {
    return{
        type: Types.IS_BACK_HOME_OPEN,
        payload: isBackHomeOpen
    }
};
