import React from 'react';
import LoaderPage from "./Components/LoaderPage";
import loadable from '@loadable/component';
import {Route} from "react-router-dom";

const ClientSide = loadable(() => import('./Containers/ClientSide'),
    {
        fallback: <LoaderPage/>
    });
const UserSide = loadable(() => import('./Containers/UserSide'),
    {
        fallback: <LoaderPage/>
    });

function App() {
    return (
        <>
            <Route path="/" component={ClientSide}/>
            <Route path="/account" component={UserSide}/>
        </>
    );
}

export default App;
