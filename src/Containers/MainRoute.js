import React from 'react';
import {Route, Switch} from "react-router-dom";
import {routesMain} from "./Routes";

function MainRoute() {
    const routeMain = routesMain.map((route, index) => {
        return <Route
                path={route.path}
                exact={route.exact}
                key={index}
                render={route.main}
                />
    });

    return (
            <Switch>
                {routeMain}
            </Switch>
    );
}

export default MainRoute;
