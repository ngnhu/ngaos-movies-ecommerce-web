import React, {useState} from 'react';
import MovieListShowing from "../Components/MovieListShowing";
import MovieListComing from "../Components/MovieListComing";

const MovieSlider = () => {
    const [isActive, setActive] = useState(true);

    const nowShowing = () => {
        setActive(true);
    };

    const comingSoon = () => {
        setActive(false)
    };

    return (
        <div className="movie__slider container__py">
            <div className="tab-bar">
                <div className={isActive ? 'tab current' : 'tab'}>
                    <button data-hover="NOW SHOWING" onClick={nowShowing}>NOW SHOWING</button>
                </div>
                <div className={!isActive ? 'tab current' : 'tab'}>
                    <button data-hover="COMING SOON" onClick={comingSoon}>COMING SOON</button>
                </div>
            </div>
            {
                isActive ?
                    <MovieListShowing/> :
                    <MovieListComing/>
            }
        </div>
    );
};

export default MovieSlider;
