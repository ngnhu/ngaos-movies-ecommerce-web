import React, {Component} from 'react';
import MenuMain from "../Components/MenuMain";
import MainRoute from "./MainRoute";
import Footer from "../Components/Footer";
import TrailerPopUp from "../Components/TrailerPopUp";
import BackToTop from "../Components/BackToTop";
import {getMovies} from "../Actions/moviesAction";
import {connect} from 'react-redux';


class ClientSide extends Component{
    componentDidMount() {
        //GET MOVIES
        this.props.getMovies();
    }

    render() {
        return (
            <div className="app">
                <MenuMain/>
                <MainRoute/>
                <Footer/>
                <TrailerPopUp/>
                <BackToTop/>
            </div>
        );
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMovies: () => {
            dispatch(getMovies());
        }
    }
};

export default connect(null, mapDispatchToProps)(ClientSide);
