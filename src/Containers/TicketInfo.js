import React, {Component} from 'react';
import {getMovieDetail} from "../Actions/moviesAction";
import {connect} from 'react-redux';

class TicketInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movieDetail: {}
        }
    }

    componentDidMount() {
        // this.props.getMovieDetail(this.props.seatReserved.id, result => {
        //     this.setState({movieDetail: result})
        // })
    }

    render() {
        const {movieDetail, ticketInfo, selectedSeat} = this.props;
        return (
            <div className="ticket-info container__order-ticket">
                <div className="ticket-info__title py_title-ticket">
                    <div className="order-title">Your information</div>
                    <div className="order-time__ticket-time">10$</div>
                </div>
                <div className="ticket-info__content">
                    <div className="ticket-info__img">
                        <img src={movieDetail.poster} alt={movieDetail.title}/>
                    </div>
                    <div className="ticket-info__text movie-detail__content">
                        <div className="info">
                            <div className="info__line">
                                <div className="title">Location</div>
                                <div
                                    className="content">{ticketInfo.cinemaId === '1' ? 'Cinema Kryst' : 'Cinema Min'}</div>
                            </div>
                            <div className="info__line">
                                <div className="title">Date</div>
                                <div className="content">{ticketInfo.dateSchedule}</div>
                            </div>
                            <div className="info__line">
                                <div className="title">Time</div>
                                <div className="content">{ticketInfo.time}</div>
                            </div>
                            <div className="info__line">
                                <div className="title">Ticket number</div>
                                <div className="content">2</div>
                            </div>
                            <div className="info__line">
                                <div className="title">Your seat</div>
                                <div className="content">{selectedSeat.seat_name}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
  return{
      selectedSeat: state.selectedSeat
  }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMovieDetail: (idMovie, callback) => {
            dispatch(getMovieDetail(idMovie, callback))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TicketInfo);
