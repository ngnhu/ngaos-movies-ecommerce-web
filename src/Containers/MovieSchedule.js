import React, {Component} from 'react';
import ShowTime from "../Components/ShowTime";
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import _ from "lodash";
import {getMovieSchedule} from "../Actions/scheduleAction";

class MovieSchedule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movieSchedule: [],
            dateSchedule: '',
            cinema: '1'
        }
    }

    componentDidMount() {
        const pathName = this.props.history.location.pathname;
        const idMovie = _.last(pathName.split("/"));
        this.props.getMovieSchedule(idMovie, (result) => {
            this.setState({
                movieSchedule: result,
                dateSchedule: this.props.movieDetail.release_date
            });
            let bgDate = document.querySelector('.movie-schedule__date');
            if (_.isEmpty(result)) {
                bgDate.style.backgroundColor = 'transparent';
            }
        });

    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render() {
        const {movieSchedule, cinema, dateSchedule} = this.state;
        let arrDateTime = [], scheduleDateTime, arrCinema = [];
        let dateMovie, cinemaSchedule;
        if (!_.isEmpty(movieSchedule)) {
            dateMovie = movieSchedule.map((d, index) => {
                if (arrDateTime.indexOf(d.date) === -1) {
                    arrDateTime.push(d.date);
                    scheduleDateTime = new Date(d.date).toDateString();
                    return <button className={dateSchedule === d.date ? 'btn-hover active' : 'btn-hover'}
                                   key={index}
                                   data-hover={scheduleDateTime}
                                   name="dateSchedule"
                                   onClick={this.onChange}
                                   value={d.date}>{scheduleDateTime}</button>
                }
                return null;
            });

            cinemaSchedule = movieSchedule.map((c, index) => {
                let cinemaString = c.cinema_id.toString();
                if (arrCinema.indexOf(cinemaString) === -1) {
                    arrCinema.push(cinemaString);
                    return <div className={cinema === cinemaString ? 'tab current' : 'tab'}
                                key={index}>
                        <button data-hover={cinemaString === '1' ? 'CINEMA KRYST' : 'CINEMA MIN'}
                                name="cinema"
                                onClick={this.onChange}
                                value={cinemaString}
                                key={index}>{cinemaString === '1' ? 'CINEMA KRYST' : 'CINEMA MIN'}</button>
                    </div>
                }
                return null;
            });
        }

        return (
            <div className="bg__movie-schedule">
                <div className="movie-schedule container-fluid py__movie-schedule text-center">
                    <div className="movie-schedule__cinema">{cinemaSchedule}</div>
                    <div className="movie-schedule__date-time">
                        <div className="movie-schedule__date">{dateMovie}</div>
                        <ShowTime dateSchedule={this.state.dateSchedule} cinema={this.state.cinema}/>
                    </div>
                </div>
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        movieDetail: state.movieDetail
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMovieSchedule: (idMovie, callback) => {
            dispatch(getMovieSchedule(idMovie, callback))
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MovieSchedule));
