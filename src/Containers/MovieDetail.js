import React, {Component} from 'react';
// import Picture from "../Components/Picture";
import {connect} from 'react-redux';
import {popUpTrailer} from "../Actions/popupAction";
import _ from 'lodash';
import {withRouter} from 'react-router-dom';
import {getMovieDetail} from "../Actions/moviesAction";

class MovieDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movieDetail: {},
            releaseDate: ""
        }
    }

    componentDidMount() {
        const pathName = this.props.history.location.pathname;
        const idMovie = _.last(pathName.split("/"));
        this.props.getMovieDetail(idMovie, (result) => {
            const formatDate = new Date(result.release_date).toDateString();

            this.setState({
                movieDetail: result,
                releaseDate: formatDate
            });
        });
    }

    openTrailer = () => {
        this.props.popUpTrailer(true, this.state.movieDetail.trailer);
    };

    render() {
        const {movieDetail, releaseDate} = this.state;
        console.log(movieDetail.rating)
        return (
            <div className="movie-detail">
                <div className="container-fluid">
                    <div className="row flex-lg-wrap">
                        <div className="col-lg-5 p-0">
                            <div id="imageDetail" className="movie-detail__img">
                                {/*<Picture src={movieDetail.poster}*/}
                                {/*         alt={movieDetail.title}/>*/}
                                <img src={movieDetail.poster} alt={movieDetail.title}/>
                                <div className="glow-wrap">
                                    <i className="glow"></i>
                                </div>
                                <button id="btnMovieDetail" onClick={this.openTrailer}></button>
                            </div>
                        </div>
                        <div className="col-lg-7">
                            <div className="movie-detail__content container__detail-content">
                                <p className="description">{movieDetail.description}</p>
                                <div className="info">
                                    <div className="info__line">
                                        <div className="title">Director</div>
                                        <div className="content">{movieDetail.director}</div>
                                    </div>
                                    <div className="info__line">
                                        <div className="title">Genre</div>
                                        <div className="content">{movieDetail.genre}</div>
                                    </div>
                                    <div className="info__line">
                                        <div className="title">Running time</div>
                                        <div className="content">{movieDetail.runtime} minutes</div>
                                    </div>
                                    <div className="info__line">
                                        <div className="title">Release date</div>
                                        <div className="content">{releaseDate}</div>
                                    </div>
                                    <div className="info__line">
                                        <div className="title">Rating</div>
                                        <div className="content">
                                            <div className="rating-icon">
                                                <span style={{width: `${movieDetail.rating * 100}%`}}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        popUpTrailer: (isTrailerOpen, urlYoutube) => {
            dispatch(popUpTrailer(isTrailerOpen, urlYoutube));
        },
        getMovieDetail: (idMovie, callback) => {
            dispatch(getMovieDetail(idMovie, callback))
        }
    }
};

export default withRouter(connect(null, mapDispatchToProps)(MovieDetail));
