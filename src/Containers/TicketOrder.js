import React, {Component} from 'react';
import OrderTime from "../Components/OrderTime";
import SeatList from "../Components/SeatList";
import OrderNote from "../Components/OrderNote";

class TicketOrder extends Component {
    render() {
        return (
            <div className="container__order-ticket">
                <OrderTime/>
                <SeatList/>
                <OrderNote/>
            </div>
        );
    }
}

export default TicketOrder;
