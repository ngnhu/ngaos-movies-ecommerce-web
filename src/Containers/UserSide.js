import React, {Component} from 'react';
import FormHeader from "../Components/FormHeader";
import FormLogo from "../Components/FormLogo";
import {Route, Switch} from "react-router-dom";
import {routesAccount} from "./Routes";

class UserSide extends Component {
    render() {
        const routeAccount = routesAccount.map((route, index) => {
            return <Route
                path={route.path}
                exact={route.exact}
                key={index}
                component={route.main}
            />
        });

        return (
            <div className="user">
                <FormHeader/>
                <FormLogo/>
                <Switch>
                    {routeAccount}
                </Switch>
            </div>
        );
    }
}

export default UserSide;
