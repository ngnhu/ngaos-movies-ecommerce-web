import React from 'react';
import loadable from '@loadable/component';
import LoaderImage from "../Components/LoaderImage";
import NewsOffers from "../Components/NewsOffers";
import CinemaMap from "../Components/CinemaMap";

// CLIENT SIDE
const Home = loadable(() => import('../Layout/HomeLayout'),
    {
        fallback: <LoaderImage/>
    });
const Movies = loadable(() => import('../Layout/MoviesLayout'),
    {
        fallback: <LoaderImage/>
    });
const MovieDetail = loadable(() => import('../Layout/MovieDetailLayout'),
    {
        fallback: <LoaderImage/>
    });
const BookTicket = loadable(() => import('../Layout/BookTicketLayout'),
    {
        fallback: <LoaderImage/>
    });
const User = loadable(() => import('../Layout/UserLayout'),
    {
        fallback: <LoaderImage/>
    });
const PageNotFoundHome = loadable(() => import('../Layout/PageNotFoundHome'),
    {
        fallback: <LoaderImage/>
    });

export const routesMain = [
    {
        path: '/',
        exact: true,
        main: (props) => <Home {...props}/>
    },
    {
        path: '/movies',
        exact: true,
        main: ({history}) => <Movies history={history}/>
    },
    {
        path: '/movies/detail/:id',
        exact: true,
        main: ({history}) => <MovieDetail history={history}/>
    },
    {
        path: '/movies/book-tickets/:cinemaId/:movieId/:showTimeId/:dateSchedule/:time',
        exact: true,
        main: (props) => <BookTicket {...props}/>
    },
    {
        path: '/user/:id',
        exact: true,
        main: () => <User/>
    },
    {
        path: '/news&offer',
        exact: true,
        main: () => <NewsOffers/>
    },
    {
        path: '/cinema',
        exact: true,
        main: () => <CinemaMap/>
    },
    {
        path: '',
        exact: true,
        main: () => <PageNotFoundHome/>
    },
];

// USER SIDE
const Login = loadable(() => import('../Components/LoginForm'),
    {
        fallback: <LoaderImage/>
    });
const SignUp = loadable(() => import('../Components/SignUpForm'),
    {
        fallback: <LoaderImage/>
    });

export const routesAccount = [
    {
        path: '/account/login',
        exact: true,
        main: ({history}) => <Login history={history}/>
    },
    {
        path: '/account/register',
        exact: true,
        main: ({history}) => <SignUp history={history}/>
    },
    {
        path: '',
        exact: false,
        main: () => <PageNotFoundHome/>
    }
];
