import React, {Component} from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.scss";
import Picture from "../Picture";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

// import loadable from "@loadable/component";
// const Picture = loadable(() => import('../Picture'));

// function SampleNextArrow(props) {
//     const {className, style, onClick} = props;
//     return (
//         <div
//             className={className}
//             // style={{...style, display: "flex", cursor: 'pointer'}}     //, display: "inline-block", fontSize: "30px"
//             style={{...style, display: "none"}}
//             onClick={onClick}
//         />
//     );
// }
//
// function SamplePrevArrow(props) {
//     const {className, style, onClick} = props;
//     return (
//         <div
//             className={className}
//             style={{...style, display: "none"}}
//             onClick={onClick}
//         />
//     );
// }

class Banner extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // dots: true,
            infinite: true,
            speed: 1500,
            slidesToShow: 1,
            slidesToScroll: 1,
            // nextArrow: <SampleNextArrow/>,
            // prevArrow: <SamplePrevArrow/>,
            // appendDots: dots => (
            //     <div className="slick-dots">{dots}</div>
            // ),
            cssEase: "cubic-bezier(0.7, 0, 0.3, 1)",
            autoplay: true,
            autoplaySpeed: 4000,
            lazyLoad: true,
            arrows: false,
            fade: 'true',
            pauseOnHover: false,
            pauseOnFocus: false

        }
    }

    render() {
        const {movies} = this.props;
        const movieTop = movies.map(movie => {
            if(movie.id === 1 || movie.id === 2 || movie.id === 14 ||movie.id === 15)
                return <div className="banner" key={movie.id}>
                    <div className="banner__detail">
                        <div className="banner__detail-text">
                            <div className="banner__detail-text__title">
                                <div className="d-flex align-items-center">
                                    <div className="line mr-3"></div>
                                    <p className="author">{movie.director}</p>
                                </div>
                                <div className="title">{movie.title}</div>
                                <p className="description">{
                                        movie.description.slice(0, 200) + '...'}
                                </p>
                            </div>
                            <div className="banner__detail-text__btn">
                                <Link className="btn btn-white-forget mr-2"
                                      to={`/movies/detail/${movie.id}`}>Detail</Link>
                                <Link to={`/movies/book-tickets/${movie.id}`} className="btn btn-beige-white">Get tickets</Link>
                            </div>
                        </div>
                    </div>
                    <div className="banner__img">
                        <Picture src={movie.poster} alt={movie.title}/>
                        {/*<img src={movie.poster} alt={movie.title}/>*/}
                    </div>
                </div>
            return 0;
        });
        return (
            <>
                <Slider {...this.state}>
                    {movieTop}
                </Slider>
            </>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        movies: state.movies
    }
};


export default connect(mapStateToProps)(Banner);
