import React, {Component} from 'react';

class NewsOffers extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <div className="news-offers square-float-white">
                <svg className="news-offers__bg-top" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
                     preserveAspectRatio="none">
                    <polygon fill="white" points="0,0 100,0 100,100"/>
                </svg>
                <ul className="circles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <h3 className="news-offers__title text-center">News & Offers</h3>
                <div className="container__movie-list">
                    <div className="row news-offers__news">
                        <div className="col-lg-6 col-12 news-detail">
                            <div className="news-detail__img">
                                <img src={require('../../Assets/Images/news2.jpg')} alt="ej"/>
                            </div>
                            <div className="inner-heading news-detail__text">
                                <h5>Blockbuster of the summer, awards season's coming!</h5>
                                <p>23 September, 2019</p>
                            </div>
                        </div>
                        <div className="col-lg-6 col-12 news-detail">
                            <div className="news-detail__img">
                                <img src={require('../../Assets/Images/joker-2.jpg')} alt="bl"/>
                            </div>
                            <div className="inner-heading news-detail__text">
                                <h5>Empire Magazine covers reveal new looks at Joaquin Phoenix's Joker...</h5>
                                <p>16 September, 2019</p>
                            </div>
                        </div>
                    </div>
                    <div className="row news-offers__offers">
                        <div className="col-lg-7 col-12 offers-detail">
                            <div className="offers-detail__img">
                                <img src={require('../../Assets/Images/imax-1.jpg')} alt="pm"/>
                                <div className="inner-heading offers-detail__img-text">
                                    <div className="offers-text">
                                        <h5>Check Out The IMAX Exclusive</h5>
                                        <p className="time">12 October 2019</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5 col-12 offers-detail">
                            <div className="offers-detail__img">
                                <img src={require('../../Assets/Images/bowl-cinema.jpg')} alt="pm"/>
                                <div className="inner-heading offers-detail__img-text">
                                    <div className="offers-text">
                                        <h5>Check Out The IMAX Exclusive</h5>
                                        <p className="time">12 October 2019</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <svg className="news-offers__bg-bottom" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
                     preserveAspectRatio="none">
                    <polygon className="svg--sm" fill="white" points="0,0 30,100 65,21 90,100 100,75 100,100 0,100"/>
                    <polygon className="svg--lg" fill="white"
                             points="0,0 15,100 33,21 45,100 50,75 55,100 72,20 85,100 95,50 100,80 100,100 0,100"/>
                </svg>
            </div>
        );
    }
}

export default NewsOffers;
