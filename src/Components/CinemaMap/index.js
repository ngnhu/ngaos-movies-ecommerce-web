import React, {Component} from 'react';
import MapContainer from "../MapContainer";

class CinemaMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isActive: true,
            isNotCurrent: 'tab',
            isCurrent: 'tab current'
        }
    }

    firstCine = () => {
        this.setState({isActive: true})
    };

    secondCine = () => {
        this.setState({isActive: false})
    };

    render() {
        return (
            <div className="movie__slider container__py">
                <div className="tab-bar">
                    <div className={this.state.isActive ? this.state.isCurrent : this.state.isNotCurrent}>
                        <button data-hover="CINEMA 1" onClick={this.firstCine}>CINEMA 1</button>
                    </div>
                    <div className={!this.state.isActive ? this.state.isCurrent : this.state.isNotCurrent}>
                        <button data-hover="CINEMA 2" onClick={this.secondCine}>CINEMA 2</button>
                    </div>
                </div>
                {
                    this.state.isActive ?
                        <MapContainer/> :
                        <MapContainer/>
                }
            </div>
        );
    }
}

export default CinemaMap;
