import React, { Component } from 'react';
import { animateScroll as scroll} from 'react-scroll';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDoubleUp} from "@fortawesome/free-solid-svg-icons";

class BackToTop extends Component {

    scrollToTop(){
        scroll.scrollToTop();
    }

    render() {
        return (
            <>
                <button className="back-to-top" onClick={this.scrollToTop}>
                    <FontAwesomeIcon icon={faAngleDoubleUp}/>
                </button>
            </>
        );
    }
}

export default BackToTop;
