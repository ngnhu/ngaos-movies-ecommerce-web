import React, {Component} from 'react';
import {connect} from 'react-redux';
import {selectedSeat} from "../../Actions/seatsAction";
import {withRouter} from 'react-router-dom';

class Seat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggleOn: false,
            ticketBooking: []
        };
        this.handleClick = this.handleClick.bind(this);
    }


    handleClick(e) {
        // const showTimeId = this.props.match.params.showTimeId;
        const seatName = e.currentTarget.dataset.seatname;
        this.setState(state => ({
            isToggleOn: !state.isToggleOn,
        }));

        if (this.state.ticketBooking.indexOf(seatName) > -1) {
            this.setState({
                ticketBooking: this.state.ticketBooking.filter(res => res !== seatName)
            })
        } else {
            this.setState({
                seatReserved: this.state.ticketBooking.concat(seatName)
            })
        }
}

classSeat = (isReserved, isToggleOn) => {
    if (isReserved === true)
        return 'seat seat-unavailable';
    if (isToggleOn === true)
        return 'seat seat-selected';
    return 'seat seat-available';
};

render()
{
    const {seat, isReserved} = this.props;
    const {isToggleOn} = this.state;

    return (
        <button className={this.classSeat(isReserved, isToggleOn)}
                disabled={isReserved}
                data-seatname={seat.seatName}
                onClick={this.handleClick}>
            {seat.seatName}
        </button>
    );
}
}

const mapStateToProps = (state) => {
    return {
        seatReserved: state.seatReserved
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectedSeat: (seat) => {
            dispatch(selectedSeat(seat))
        }
    }
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Seat));
