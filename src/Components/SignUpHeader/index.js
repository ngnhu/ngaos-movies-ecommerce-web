import React, {Component} from 'react';
import {Link} from "react-router-dom";


class SignUpHeader extends Component {
    render() {
        return (
            <div className="form-header__right">
                <p>Don't have an account yet?</p>
                <Link to="/account/register" className="btn btn-black">Sign up</Link>
            </div>
        );
    }
}

export default SignUpHeader;
