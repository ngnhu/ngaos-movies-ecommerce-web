import React, {Component} from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft, faArrowRight} from "@fortawesome/free-solid-svg-icons";
import MovieComingSoon from "../MovieComingSoon";
import {connect} from 'react-redux';


function NextArrow(props) {
    const {className, style, onClick} = props;
    return (
        <div className={className} style={{...style, display: "block"}} onClick={onClick}>
            <FontAwesomeIcon icon={faArrowRight}/>
        </div>
    );
}

function PrevArrow(props) {
    const {className, style, onClick} = props;
    return (
        <div className={className} style={{...style, display: "block"}} onClick={onClick}>
            <FontAwesomeIcon icon={faArrowLeft}/>
        </div>
    );
}

class MovieListComing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            infinite: true,
            speed: 900,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrow: true,
            nextArrow: <NextArrow/>,
            prevArrow: <PrevArrow/>,
            cssEase: "cubic-bezier(0.7, 0, 0.3, 1)",
            autoplay: false,
            lazyLoad: true,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 577,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        }
    }

    render() {
        const {movies} = this.props;
        const movieComing = movies.map((movie) => {
            if(movie._now_showing === false)
                return <MovieComingSoon movie={movie} key={movie.id}/>
            return 0;
        });

        return (
            <div className="movie-list container__movie-list">
                <Slider {...this.state}>
                    {movieComing}
                </Slider>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        movies: state.movies
    }
};

export default connect(mapStateToProps)(MovieListComing);
