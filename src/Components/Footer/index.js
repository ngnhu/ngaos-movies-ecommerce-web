import React from 'react';
import {faEnvelope} from "@fortawesome/free-regular-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faFacebookSquare, faGit, faLinkedin,
    faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import {Link} from "react-router-dom";

const Footer = () => {
    const linkFacebook = "https://www.facebook.com/quynhnhu134";
    const linkGit = "https://github.com/ngnhu/ngaos-movie-web";
    const linkLinked = "https://www.linkedin.com/in/nhu-nguyen-079448193?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BbJK5udQpRQSv4XCQzXn6%2BA%3D%3D";
    return (
        <div className="footer container__main">
            <div className="row footer__line">
                <div className="logo-page mr-5">
                    <a href="/">NGAOS</a>
                </div>
                <div className="footer__register">
                    <Link className="footer__register-link link-sinbad" to="/account/register" data-content="SIGN UP OUR NEWSLETTER">
                        <span>SIGN UP OUR NEWSLETTER</span>
                    </Link>
                    <FontAwesomeIcon className="register_icon" icon={faEnvelope}/>
                </div>
            </div>
            <hr/>
            <div className="row footer__line">
                <div className="intro mr-5">© 2019 NGAOS.</div>
                <div className="footer__social">
                    <a href={linkFacebook} target="_blank" rel="noopener noreferrer">
                        <FontAwesomeIcon className="footer__social-icon ml-0" icon={faFacebookSquare}/>
                    </a>
                    <a href={linkGit} target="_blank" rel="noopener noreferrer">
                        <FontAwesomeIcon className="footer__social-icon" icon={faGit}/>
                    </a>
                    <a href={linkLinked} target="_blank" rel="noopener noreferrer">
                        <FontAwesomeIcon className="footer__social-icon" icon={faLinkedin}/>
                    </a>
                    <FontAwesomeIcon className="footer__social-icon" icon={faTwitter}/>
                </div>
            </div>
        </div>
    );
};

export default Footer;
