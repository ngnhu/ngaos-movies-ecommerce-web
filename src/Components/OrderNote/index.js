import React, {Component} from 'react';

class OrderNote extends Component {
    render() {
        return (
            <div className="order-note">
                <div className="row">
                    <div className="col-md-4 col-12">
                        <div className="order-note__content">
                            <div className="seat-available"></div>
                            <p>Available</p>
                        </div>
                    </div>
                    <div className="col-md-4 col-12">
                        <div className="order-note__content">
                            <div className="seat-unavailable"></div>
                            <p>Unavailable</p>
                        </div>
                    </div>
                    <div className="col-md-4 col-12">
                        <div className="order-note__content">
                            <div className="seat-selected"></div>
                            <p>Selected</p>
                        </div>
                    </div>
                </div>
                <div className="order-note__notice">
                    <h4>Notice:</h4>
                    <p>*You couldn’t cancel and change or charge money about your ticket was bought.</p>
                    <p>*Code ticket will be sent through SMS and your email.</p>
                </div>
            </div>
        );
    }
}

export default OrderNote;
