import React, {Component} from 'react';
import {register} from "../../Actions/userAction";
import FormError from "../FormError";

class SignUpForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dob: {
                value: '',
                isInputValid: true,
                errorMessage: ''
            },
            email: '',
            full_name: '',
            password: {
                value: '',
                isInputValid: true,
                errorMessage: ''
            },
            phone: {
                value: '',
                isInputValid: true,
                errorMessage: ''
            },
            username: {
                value: '',
                isInputValid: true,
                errorMessage: ''
            }
        }
    }

    validateInput = (type, checkingText) => {
        switch (type) {
            case "dob":
                const regexDate =/^(((0)[0-9])|((1)[0-2]))(\/)([0-2][0-9]|(3)[0-1])(\/)\d{4}$/;
                const checkingDob = regexDate.exec(checkingText);
                if (checkingDob !== null) {
                    return {
                        isInputValid: true,
                        errorMessage: ''
                    };
                } else {
                    return {
                        isInputValid: false,
                        errorMessage: 'Your date is not mm/dd/yyyy type.'};
                }

            case "username":
                const regexUser = /^[a-zA-Z0-9]+([._]?[a-zA-Z0-9]+)*$/;
                const checkingUser = regexUser.exec(checkingText);
                if (checkingUser !== null) {
                    return {
                        isInputValid: true,
                        errorMessage: ''
                    };
                } else {
                    return {
                        isInputValid: false,
                        errorMessage: "Your username can be contained alpha, figure, dot, underscore"};
                }

            case "password":
                if (checkingText.length >= 6) {
                    return {
                        isInputValid: true,
                        errorMessage: ''
                    };
                } else {
                    return {
                        isInputValid: false,
                        errorMessage: 'Your password at least 6 characters.'
                    };
                }

            default:
                break;
        }
    };

    handleInput = event => {
        const {name, value} = event.target;
        const newState = {...this.state[name]}; /* dummy object */
        newState.value = value;
        this.setState({[name]: newState});
    };

    handleInputValidation = event => {
        const {name} = event.target;
        const {isInputValid, errorMessage} = this.validateInput(name, this.state[name].value);
        const newState = {...this.state[name]}; /* dummy object */
        newState.isInputValid = isInputValid;
        newState.errorMessage = errorMessage;
        this.setState({[name]: newState})
    };

    isSubmit = () => {
        if(this.state.dob.isInputValid && this.state.password.isInputValid && this.state.username.isInputValid)
            return true;
        return false
    };

    onSubmit = (e) => {
        e.preventDefault();
        const userRegister =
        {
            dob: this.state.dob.value,
            email: this.state.email.value,
            full_name: this.state.full_name.value,
            password: this.state.password.value,
            phone: this.state.phone.value,
            username: this.state.username.value
        };

        if (this.isSubmit) {
            register(userRegister, () => {
                this.props.history.push('/account/login')
            });
            console.log(userRegister)
        } else {
            console.log('error')
        }

    };


    render() {
        return (
            <div className="container__form">
                <div className="text-center mb-md-5 mb-4">
                    <p>Get better services with an account.</p>
                </div>
                <form id="form-signUp" className="form__content" onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label htmlFor="full_name">Full Name</label>
                        <input type="text" className="form-control" id="full_name" autoFocus required
                               name="full_name"
                               placeholder="Jenny Yang"
                               onChange={this.handleInput}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="dob">Date of birth</label>
                        <input type="text" className="form-control" id="dob" required
                               name="dob"
                               placeholder="mm/dd/yyyy"
                               onChange={this.handleInput}
                               onBlur={this.handleInputValidation}/>
                        <FormError type="dob"
                                   isHidden={this.state.dob.isInputValid}
                                   errorMessage={this.state.dob.errorMessage}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="phone">Phone</label>
                        <input type="number" className="form-control" id="phone" required
                               name="phone"
                               placeholder="+84 933 759 348"
                               onChange={this.handleInput}/>
                        <FormError type="phone"
                                   isHidden={this.state.phone.isInputValid}
                                   errorMessage={this.state.phone.errorMessage}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" id="username" required
                               name="username"
                               placeholder="jenny.yang"
                               onChange={this.handleInput}
                               onBlur={this.handleInputValidation}/>
                        <FormError type="username"
                                   isHidden={this.state.username.isInputValid}
                                   errorMessage={this.state.username.errorMessage}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input type="email" className="form-control" id="email" required
                               name="email"
                               placeholder="ngaos@gmail.com"
                               onChange={this.handleInput}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" id="password" required
                               name="password"
                               placeholder="At least 6 characters"
                               onChange={this.handleInput}
                               onBlur={this.handleInputValidation}/>
                        <FormError type="password"
                                   isHidden={this.state.password.isInputValid}
                                   errorMessage={this.state.password.errorMessage}/>
                    </div>
                    <button type="submit" className="btn btn-form-submit btn-block">Create my free account</button>
                </form>
            </div>
        );
    }
}

export default SignUpForm;
