import React, {Component} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import SignUpHeader from "../SignUpHeader";
import LoginHeader from "../LoginHeader";
import _ from 'lodash';
import {withRouter} from 'react-router-dom';

class FormHeader extends Component {
    render() {
        const pathName = this.props.history.location.pathname;
        const pageName = _.last(pathName.split("/"));

        return (
            <div className="form-header container-fluid">
                <div className="row justify-content-between align-items-center container__main form-header__content ">
                    <Link to={`/`} className="btn btn-form-home">
                        <FontAwesomeIcon icon={faChevronLeft}/>
                    </Link>
                    {
                        pageName === 'login' ? <SignUpHeader/> : <LoginHeader/>
                    }
                </div>
            </div>
        );
    }
};

export default withRouter(FormHeader);
