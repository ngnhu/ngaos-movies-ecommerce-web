import React, {Component} from 'react';

class FormError extends Component {
    render() {
        const {isHidden, errorMessage} = this.props;
        return (
            <>
                {
                    !isHidden ?
                        <div className="text-danger">
                            {errorMessage}
                        </div>:
                        ''
                }
            </>
        );
    }
}

export default FormError;
