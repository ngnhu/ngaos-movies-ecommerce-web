import React from 'react';
import {connect} from 'react-redux';
import MovieNowShowing from "../MovieNowShowing";

const AllMoviesShowing = (props) => {
    const {movies} = props;
    const movieShowing = movies.map((movie, index) => {
        if(movie._now_showing === true)
            return <div className="col-xl-3 col-lg-4 col-md-6 mt-md-5 mt-3"
                        key={index}>
                <MovieNowShowing movie={movie}/>
            </div>
        return null;
    });

    return (
        <div className="row movie-list container__movie-list">
            {movieShowing}
        </div>
    );
};
const mapStateToProps = (state) => {
    return {
        movies: state.movies
    }
};


export default connect(mapStateToProps)(AllMoviesShowing);
