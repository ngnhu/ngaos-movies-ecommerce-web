import React, {Component} from 'react';
import {Link} from "react-router-dom";

class LoginHeader extends Component {
    render() {
        return (
            <div className="form-header__right">
                <p>Already have an account?</p>
                <Link to="/account/login" className="btn btn-white">Login</Link>
            </div>
        );
    }
}

export default LoginHeader;
