import React from 'react';

const NoShowtime = () => {
    return (
        <div className="no-showtime">
            <div className="no-showtime__text">Sorry there is no showtime, please comeback later.</div>
        </div>
    );
};

export default NoShowtime;
