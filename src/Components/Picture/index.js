import React from "react";

class Picture extends React.PureComponent {
    componentDidMount() {
        require('intersection-observer');
        this.observer = new IntersectionObserver(
            entries => {
                entries.forEach(entry => {
                    const { isIntersecting } = entry;

                    if (isIntersecting) {
                        this.element.src = this.props.src;
                        this.element.alt = this.props.alt;
                        this.element.className = this.props.className;
                        this.observer = this.observer.disconnect();
                    }
                });
            },
            {
                root: document.querySelector("body"),
            }
        );

        this.observer.observe(this.element);
    }

    render() {
        return <img ref={el => this.element = el} alt=""/>;
    }
}

export default Picture;
