import React, {Component} from 'react';
import {login, setCurrentUser} from "../../Actions/userAction";
import {connect} from 'react-redux';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: "",
            usernameOrEmail: ""
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };

    onSubmit = (e) => {
        e.preventDefault();
        login(this.state, (result) => {
            this.props.setCurrentUser(result);
            this.props.history.push('/');
        });
    };


    render() {
        return (
            <div className="container__form">
                <div className="text-center mb-md-5 mb-4">
                    <p>Hello, who's this?</p>
                </div>
                <form id="form-login" className="form__content"
                      onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label htmlFor="email">Username or Email</label>
                        <input type="text" className="form-control" id="email" autoFocus required
                               placeholder="jenny.yang or ngaos@gmail.com"
                               name="usernameOrEmail"
                               onChange={this.onChange}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" id="password" required
                               placeholder="At least 6 characters"
                               name="password"
                               onChange={this.onChange}/>
                    </div>
                    <button type="submit" className="btn btn-form-submit btn-block">Login to NGAOS</button>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        setCurrentUser: (data) => {
            dispatch(setCurrentUser(data))
        }
    }
};

export default connect(null, mapDispatchToProps)(LoginForm);
