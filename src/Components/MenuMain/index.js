import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUserCircle} from "@fortawesome/free-regular-svg-icons";
import {Link, NavLink} from "react-router-dom";
import Picture from "../Picture";
import {withRouter} from 'react-router-dom';



function MenuMain() {
    return (
        <>
            <div id="menu" className="container-fluid">
                <div className="row justify-content-between align-items-center container__main">
                    <div className="menu-icon">
                        <span className="menu-icon__line menu-icon__line-left"></span>
                        <span className="menu-icon__line"></span>
                        <span className="menu-icon__line menu-icon__line-right"></span>
                    </div>
                    <div className="menu-logo logo-page">
                        <Link to="/">NGAOS</Link>
                    </div>
                    <div className="menu-user">
                        <div className="menu-user__icon">
                            <FontAwesomeIcon className="menu-user__icon-size" icon={faUserCircle}/>
                        </div>
                        <div className="menu-user__acc">
                            <div className="position-relative w-75 mx-auto">
                                <NavLink to={`/user/${'user123'}`} className="menu-user__acc-name btn-hover btn-close" activeClassName="active">Anonymous</NavLink>
                            </div>
                            <div className="row justify-content-center">
                                <Link to={`/account/login`} className="btn btn-black mr-1 btn-close">
                                    Login
                                </Link>
                                <Link to={`/account/register`} className="btn btn-black ml-1 btn-close">
                                    Sign Up
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="menu-main">
                <div className="menu-main__content">
                    <ul className="menu-main__list">
                        <li className="menu-main__list-item">
                            <NavLink exact to="/" className="btn-hover" activeClassName="active">Home</NavLink>
                        </li>
                        <li className="menu-main__list-item">
                            <NavLink exact to="/movies" className="btn-hover" activeClassName="active">Movies</NavLink>
                        </li>
                        <li className="menu-main__list-item">
                            <NavLink exact to="/news&offer" className="btn-hover" activeClassName="active">News & Offers</NavLink>
                        </li>
                        <li className="menu-main__list-item">
                            <NavLink exact to="/cinema" className="btn-hover" href="#hdf">Cinema</NavLink>
                        </li>
                    </ul>
                </div>
            </div>
            <Picture className="bg_banner-img" src={require('../../Assets/Images/blob__shape-header.svg')} alt=""/>
            {/*<div className="bg-index">*/}
            {/*</div>*/}
        </>
    );
}

export default withRouter(MenuMain);
