import React, {Component} from 'react';
import {connect} from 'react-redux';
import {popUpBackHome} from "../../Actions/popupAction";

class OrderTime extends Component {
    constructor(props) {
        super(props);
        this.state = {
            seconds: '00',
            minutes: '5'
        };

        this.secondsRemaining = '';
        this.intervalHandle = '';
        // method that triggers the countdown functionality
        this.startCountDown = this.startCountDown.bind(this);
        this.tick = this.tick.bind(this);
    }

    tick() {
        let min = Math.floor(this.secondsRemaining / 60);
        let sec = this.secondsRemaining - (min * 60);
        this.setState({
            minutes: min,
            seconds: sec
        });

        if (sec < 10) {
            this.setState({
                seconds: "0" + this.state.seconds,
            })
        }
        // if (min < 10) {
        //     this.setState({
        //         value: "0" + min,
        //     })
        // }
        if (min === 0 && sec === 0) {
            clearInterval(this.intervalHandle);
            this.props.popUpBackHome(true);
        }
        this.secondsRemaining--;
    }

    startCountDown() {
        this.intervalHandle = setInterval(this.tick, 1000);
        let time = this.state.minutes;
        this.secondsRemaining = time * 60;
    }

    componentDidMount() {
        this.startCountDown();
    }
    componentWillUnmount() {
        clearInterval(this.intervalHandle);
    }

    render() {
        const {seconds, minutes} = this.state;

        return (
            <div className="order-time">
                <div className="order-time__ticket py_title-ticket">
                    <div className="order-title">Order your ticket</div>
                    <div className="order-time__ticket-time">{minutes}:{seconds} min</div>
                </div>
                <div className="order-time__screen">
                    <div className="screen__title">SCREEN</div>
                    <div className="screen__line"></div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        popUpBackHome: (isBackHomeOpen) => {
            dispatch(popUpBackHome(isBackHomeOpen));
        },
    }
};

export default connect(null, mapDispatchToProps)(OrderTime);
