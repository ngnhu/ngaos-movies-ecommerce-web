import React from 'react';

const LoaderImage = () => {
    return (
        <div className="loader-main-route">
            <div className="loader-main-route_boxes">
                <div className="box"></div>
                <div className="box"></div>
            </div>
        </div>
    );
};

export default LoaderImage;
