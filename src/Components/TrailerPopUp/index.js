import React from 'react';
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {connect} from 'react-redux';
import {popUpTrailer} from "../../Actions/popupAction";
import ReactPlayer from 'react-player'

const TrailerPopUp = (props) => {

    const closeTrailer = () => {
        props.popUpTrailer(false, '');
    };

    const {popup} = props;

    return (
        <>
            <div className={popup.isTrailerOpen === true ? 'trailer' : 'trailer-hidden'}>
                <div className="trailer__container">
                    {/*<iframe width="100%" height="100%" src={this.props.popup.isTrailerOpen ? this.state.apiYoutube : ''} frameBorder="0"*/}
                    {/*        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"*/}
                    {/*        allowFullScreen title="IT 2" scrolling="no">*/}
                    {/*</iframe>*/}
                    <ReactPlayer url={popup.isTrailerOpen ? popup.urlYoutube : ''}
                                 playing
                                 controls/>
                    <button className="trailer__btn-close" onClick={closeTrailer}>
                        <FontAwesomeIcon icon={faTimes}/>
                    </button>
                </div>
            </div>
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        popup: state.popup
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        popUpTrailer: (isTrailerOpen, urlYoutube) => {
            dispatch(popUpTrailer(isTrailerOpen, urlYoutube));
        },
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(TrailerPopUp);
