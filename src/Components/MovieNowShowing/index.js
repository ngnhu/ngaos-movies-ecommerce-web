import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFilm, faTicketAlt} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import loadable from "@loadable/component";
import {connect} from 'react-redux';
import {popUpTrailer} from "../../Actions/popupAction";


const Picture = loadable(() => import('../Picture'));


const MovieNowShowing = (props) => {

    const openTrailer = () => {
        props.popUpTrailer(true, movie.trailer);
    };

    // const openDetail = () => {
    //     props.popUpDetail(true);
    // };

    const {movie} = props;

    return (
        <>
            <div className="movie">
                <Picture src={movie.poster} alt="IT: Chapter Two"/>
                {/*<img src={movie.poster} alt={movie.title}/>*/}
                <div className="movie__overlay"></div>
                {/*<div className="movie__detail">*/}
                {/*    <Link to={`/movies/detail/${movie.id}`} className="circle-white-sinbad italic">*/}
                {/*        <FontAwesomeIcon className="circle__icon" icon={faArrowRight}/>*/}
                {/*    </Link>*/}
                {/*</div>*/}
                <div className="movie__content">
                    <h1>{movie.title}</h1>
                    <p>{movie.runtime} minutes</p>
                    <div className="movie__content-btn">
                        <button className="circle-white-sinbad" onClick={openTrailer}
                                style={{marginRight: '3rem'}}>
                            <FontAwesomeIcon className="circle__icon" icon={faFilm}/>
                        </button>
                        <Link to={`/movies/detail/${movie.id}`} className="circle-sinbad-white">
                            <FontAwesomeIcon className="circle__icon" icon={faTicketAlt}/>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        popUpTrailer: (isTrailerOpen, urlYoutube) => {
            dispatch(popUpTrailer(isTrailerOpen, urlYoutube));
        },
        // popUpDetail: (isDetailOpen) => {
        //     dispatch(popUpDetail(isDetailOpen));
        // }
    }
};

export default connect(null, mapDispatchToProps)(MovieNowShowing);
