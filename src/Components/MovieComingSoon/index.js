import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFilm, faTicketAlt} from "@fortawesome/free-solid-svg-icons";
import Picture from "../Picture";
import {connect} from 'react-redux';
import {popUpTrailer} from "../../Actions/popupAction";
import {Link} from "react-router-dom";

const MovieComingSoon = (props) => {
    const openTrailer = () => {
        props.popUpTrailer(true, movie.trailer);
    };

    // const openDetail = () => {
    //     props.popUpDetail(true);
    //     //Link youtube takes here
    // };

    const {movie} = props;

    return (
        <div className="movie">
            <Picture src={movie.poster}
                     alt={movie.title}/>
            {/*<img src={require('../../Assets/Images/poster_racing-in-the-rain.jpg')} alt="IT: Chapter Two"/>*/}
            <div className="movie__overlay"></div>
            {/*<div className="movie__detail">*/}
            {/*    <button className="circle-white-sinbad italic"*/}
            {/*            onClick={openDetail}>*/}
            {/*        <FontAwesomeIcon className="circle__icon" icon={faArrowRight}/>*/}
            {/*    </button>*/}
            {/*</div>*/}
            <div className="movie__content">
                <h1>{movie.title}</h1>
                <p>{movie.runtime} minutes</p>
                <div className="movie__content-btn">
                    <button className="circle-white-sinbad" style={{marginRight: '3rem'}}
                            onClick={openTrailer}>
                        <FontAwesomeIcon className="circle__icon" icon={faFilm}/>
                    </button>
                    <Link className="circle-sinbad-white"
                          to={`/movies/detail/${movie.id}`}>
                        <FontAwesomeIcon className="circle__icon" icon={faTicketAlt}/>
                    </Link>
                </div>
            </div>
        </div>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        popUpTrailer: (isTrailerOpen, urlYoutube) => {
            dispatch(popUpTrailer(isTrailerOpen, urlYoutube));
        },
        // popUpDetail: (isDetailOpen) => {
        //     dispatch(popUpDetail(isDetailOpen));
        // }
    }
};

export default connect(null, mapDispatchToProps)(MovieComingSoon);
