import React from 'react';
import {connect} from 'react-redux';
import MovieComingSoon from "../MovieComingSoon";

const AllMoviesComing = (props) => {
    const {movies} = props;
    const movieComing = movies.map((movie, index) => {
        if(movie._now_showing === false)
            return <div className="col-xl-3 col-lg-4 col-md-6 mt-md-5 mt-3"
                    key={index}>
                <MovieComingSoon movie={movie}/>
            </div>
        return null;
    });
    return (
        <div className="row movie-list container__movie-list">
            {movieComing}
        </div>
    );
};
const mapStateToProps = (state) => {
    return {
        movies: state.movies
    }
};

export default connect(mapStateToProps)(AllMoviesComing);
