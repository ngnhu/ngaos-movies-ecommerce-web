import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {popUpBackHome} from "../../Actions/popupAction";
// import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
// import {faTimes} from "@fortawesome/free-solid-svg-icons";


class BackHomePopUp extends React.Component {
    goBack = () => {
        this.props.history.goBack();
        this.props.popUpBackHome(false);
    };

    render() {
        return (
            <div>
                <div className={this.props.popup.isBackHomeOpen ? 'popup-appear' : 'popup-hidden'}>
                    <div className={this.props.popup.isBackHomeOpen ? 'popup-inner-appear' : 'popup-inner-hidden'}>
                        <div className="back-home">
                            <h3>Oops! TIME OUT</h3>
                            <div className="back-home__text">
                                Please, go back to get another chance and quickly book your own tickets.
                            </div>
                            <button className="back-home__btn btn-pink-white" onClick={this.goBack}>
                                GO BACK
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        popup: state.popup
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        popUpBackHome: (isBackHomeOpen) => {
            dispatch(popUpBackHome(isBackHomeOpen))
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BackHomePopUp));
