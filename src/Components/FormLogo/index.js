import React, {Component} from 'react';
import {Link} from "react-router-dom";

class FormLogo extends Component {
    render() {
        return (
            <div className="form-logo">
                <div className="form-logo__bg">
                    <div className="form-logo__title logo-page">
                        <Link to="/">NGAOS</Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default FormLogo;
