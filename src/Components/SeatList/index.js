import React, {Component} from 'react';
import Seat from "../Seat";
import {dataSeat} from "../../Assets/Custom/dataSeat";
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import _ from "lodash";

class SeatList extends Component {
    isReserved = (seatName) => {
        if(!_.isEmpty(this.props.seatReserved)) {
            const arrSeatReserved = this.props.seatReserved.filter(seat => seat.seat_name === seatName);
            if (arrSeatReserved.length !== 0)
                return true;
        }
        return false;
    };

    render() {
        const seatList = dataSeat.map((seat, index) => {
            return <div className="col-2" key={index}>
                <Seat seat={seat} isReserved={this.isReserved(seat.seatName)}/>
            </div>
        });

        return (
            <div className="seat-list">
                <div className="row">
                    {seatList}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        seatReserved: state.seatReserved
    }
};

export default withRouter(connect(mapStateToProps)(SeatList));
