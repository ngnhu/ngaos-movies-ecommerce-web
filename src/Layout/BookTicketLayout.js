import React, {Component} from 'react';
import TicketOrder from "../Containers/TicketOrder";
import TicketInfo from "../Containers/TicketInfo";
import {getSeatReversed} from "../Actions/seatsAction";
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {getMovieDetail} from "../Actions/moviesAction";
import {getMovieSchedule} from "../Actions/scheduleAction";
import BackHomePopUp from "../Components/BackHomePopUp";

class BookTicketLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            seatReserved: [],
            movieDetail: {},
            ticketInfo: {
                cinemaId: '',
                dateSchedule: '',
                time: ''
            }
        }
    }

    componentDidMount() {
        const cinemaId = this.props.match.params.cinemaId;
        const showTimeId = this.props.match.params.showTimeId;
        const movieId = this.props.match.params.movieId;
        const dateSchedule = new Date(this.props.match.params.dateSchedule).toDateString();
        const time = this.props.match.params.time;
        this.props.seatReserved(showTimeId, result => {
            this.setState({seatReserved: result})
        });
        this.props.getMovieDetail(movieId, result => {
            this.setState({
                movieDetail: result,
                ticketInfo: {
                    cinemaId,
                    dateSchedule,
                    time
                }
            })
        });
        this.props.getMovieSchedule(movieId);
    }

    render() {
        const {seatReserved, movieDetail, ticketInfo} = this.state;
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-7 ticket-order">
                        <TicketOrder/>
                    </div>
                    <div className="col-lg-5">
                        <TicketInfo seatReserved={seatReserved} movieDetail={movieDetail} ticketInfo={ticketInfo}/>
                    </div>
                </div>
                <BackHomePopUp/>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        seatReserved: (showTimeId, callback) => {
            dispatch(getSeatReversed(showTimeId, callback))
        },
        getMovieDetail: (movieId, callback) => {
            dispatch(getMovieDetail(movieId, callback))
        },
        getMovieSchedule: (movieId, callback) => {
            dispatch(getMovieSchedule(movieId, callback))
        }
    }
};

export default withRouter(connect(null, mapDispatchToProps)(BookTicketLayout));
