import React, {useState} from 'react';
import AllMoviesShowing from "../Components/AllMoviesShowing";
import AllMoviesComing from "../Components/AllMoviesComing";

const MoviesLayout = () => {
    const [isActive, setActive] = useState(true);

    const nowShowing = () => {
        setActive(true);
    };

    const comingSoon = () => {
        setActive(false)
    };
    return (
        <div className="movie__slider container__py">
            <div className="tab-bar">
                <div className={isActive ? 'tab current' : 'tab'}>
                    <button data-hover="NOW SHOWING" onClick={nowShowing}>NOW SHOWING</button>
                </div>
                <div className={!isActive ? 'tab current' : 'tab'}>
                    <button data-hover="COMING SOON" onClick={comingSoon}>COMING SOON</button>
                </div>
            </div>
            {
                isActive ?
                    <AllMoviesShowing/> :
                    <AllMoviesComing/>
            }
        </div>
    );
}

export default MoviesLayout;
