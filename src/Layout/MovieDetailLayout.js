import React, {Component} from 'react';
import MovieDetail from "../Containers/MovieDetail";
import MovieSchedule from "../Containers/MovieSchedule";

class MovieDetailLayout extends Component {
    render() {
        return (
            <div className="main-page__animated">
                    <MovieDetail/>
                    <MovieSchedule/>
            </div>
        );
    }
}

export default MovieDetailLayout;
