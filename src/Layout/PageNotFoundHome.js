import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import _ from "lodash";

class PageNotFoundHome extends Component {

    render() {
        const pathName = this.props.history.location.pathname;
        const pageName = _.last(pathName.split("/"));

        return (
            <div className="found-home">
                <div className="bubble-pink"></div>
                <div className="bubble-pink"></div>
                <div className="bubble-pink"></div>
                <div className="bubble-pink"></div>
                <div className="bubble-pink"></div>
                <div className="bubble-pink"></div>
                <Link className="found-home__link link-sinbad" to="/" data-content="LOST IN NGAOS">
                    <span>LOST IN NGAOS</span>
                </Link>
                <div className="text-404">
                    <h1>404</h1>
                    <p>LOST IN <span>NGAOS</span> {pageName}? Hmm, looks like that page doesn't exist.
                    </p>
                </div>
            </div>
        );
    }
}

export default withRouter(PageNotFoundHome);
