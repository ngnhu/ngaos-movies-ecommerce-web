import React from 'react';
import MovieSlider from "../Containers/MovieSlider";
import Banner from "../Components/Banner";
import NewsOffers from "../Components/NewsOffers";
import CinemaMap from "../Components/CinemaMap";

const HomeLayout = () => {
    return (
        <div className="main-page__animated">
            <Banner/>
            <MovieSlider/>
            <NewsOffers/>
            <CinemaMap/>
        </div>
    );
};
export default HomeLayout;
